/*
 * @Description: 配置文件
 * @Author: hai-27
 * @Date: 2020-02-07 16:23:00
 * @LastEditors: hai-27
 * @LastEditTime: 2021-03-03 22:32:57
 */
module.exports = {
  publicPath: './',
  devServer: {
    open: true,
    proxy: {
      "/test": {
        // target: 'http://www.xiaomi.com/', // 本地后端地址
        // target: 'http://chblzh.g21computerczc.cn/', // 线上后端地址
        target: 'http://43.138.146.146/', // 线上后端地址
        changeOrigin: true, //允许跨域
      }
    }
  }
}